import dashboard from './dashboard';
import landManage from './LandManage';

// ==============================|| MENU ITEMS ||============================== //

const menuItems = {
  // thêm menu
  items: [dashboard, landManage]
};

export default menuItems;
