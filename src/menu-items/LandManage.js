// assets
import { IconHexagon3d, IconMap2 } from '@tabler/icons';

// constant
const icons = { IconHexagon3d, IconMap2 };

// ==============================|| DASHBOARD MENU ITEMS ||============================== //

const landManage = {
  id: 'landManage',
  title: 'Quỹ Đất',
  type: 'group',
  children: [
    {
      id: 'quanLyQuyDat',
      title: 'Quản lý quỹ đất',
      type: 'item',
      url: '/land-manage/land-mark',
      icon: icons.IconHexagon3d,
      breadcrumbs: false
    },
    {
      id: 'banDo',
      title: 'Bản đồ',
      type: 'item',
      url: '/landManage/banDo',
      icon: icons.IconMap2,
      breadcrumbs: false
    }
  ]
};

export default landManage;
